import React from 'react';
import Main from '../../client/components/Main';
import renderer from 'react-test-renderer';

it('should render Main', () => {
	const dom = renderer.create(
		<Main>
			<div>i bims der div</div>
		</Main>
	).toJSON();
	expect(dom).toMatchSnapshot();
});