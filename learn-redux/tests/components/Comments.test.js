import React from 'react'
import renderer from 'react-test-renderer'
import configureMockStore from 'redux-mock-store'
import Comments from '../../client/components/Comments'
import {Provider} from 'react-redux'
import postComments from '../../client/data/comments'

const params = {photoId: 'BAhvZrRwcfu'}
const mockStore = configureMockStore([])
const store = mockStore({})

it('renders correctly', () => {
	const domTree = renderer.create(
		<Provider store={store}>
			<Comments
				postComments={postComments['BAhvZrRwcfu']}
				removeComment={() => {}}
				params={params}
			/>
		</Provider>
	).toJSON()
	expect(domTree).toMatchSnapshot()
})