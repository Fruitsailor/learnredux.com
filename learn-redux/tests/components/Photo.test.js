import React from 'react';
import Photo from '../../client/components/Photo';
import renderer from 'react-test-renderer';

it('shouldRenderPhoto', () => {
	const dom = renderer.create(<Photo post={[]} i={5} comments={[]} increment={() => {}}/>).toJSON();
	expect(dom).toMatchSnapshot();
});