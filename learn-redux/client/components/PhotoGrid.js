import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import Photo from './Photo';

const styles = {
    photoGrid: {
        display: 'flex',
        flexWrap: 'wrap',
        maxWidth: '1200px',
        margin: '0 auto',
    }
};

const PhotoGrid = (props) => (
    <div className={props.classes.photoGrid}>
        {props.posts.map((post, i) => <Photo {...props} key={i} i={i} post={post} />)}
    </div>
);
PhotoGrid.PropTypes = {
    posts: PropTypes.array.isRequired,
    classes: PropTypes.array.isRequired,
};

export default injectSheet(styles)(PhotoGrid);
