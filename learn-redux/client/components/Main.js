import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';

const Main = (props) => (
    <div>
        <h1>
            <Link to='/'>Reduxstagram</Link>
        </h1>
        { React.cloneElement(props.children, props) }
    </div>
);

Main.PropTypes = {
    children: PropTypes.node.isRequired,
}

export default Main;