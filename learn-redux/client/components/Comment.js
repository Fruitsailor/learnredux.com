import React from 'react';
import PropTypes from 'prop-types';

const Comment = (props) => (
    <div className='comment'>
        <p>
            <strong>{props.comment.user}</strong>
            {props.comment.text}
            <button className='remove-comment' onClick={() => props.removeComment(props.postId, props.i)} >&times;</button>
        </p>
    </div>
)

Comment.PropTypes = {
    removeComment: PropTypes.func.isRequired,
    postId: PropTypes.string.isRequired,
    comment: PropTypes.shape({
        user: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired,
    }),
    i: PropTypes.number.isRequired,
}

export default Comment;