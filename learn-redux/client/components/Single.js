import React from 'react';
import { reset } from 'redux-form';
import Photo from './Photo';
import Comments from './Comments';

class Single extends React.Component {

    handleCommentsSubmit = (values, dispatch) => {
        this.props.addComment(this.props.params.postId, values.author, values.comment);
        dispatch(reset('commentForm'));
    }

    render() {
        const { postId } = this.props.params;
        const i = this.props.posts.findIndex(post => post.code === postId);
        const post = this.props.posts[i];
        const postComments = this.props.comments[postId] || [];
        return (
            <div className='single-photo'>
                <Photo i={i} post={post} {...this.props} />
                <Comments postComments={postComments} onSubmit={this.handleCommentsSubmit} {...this.props} ></Comments>
            </div>
        );
    }
};

export default Single;