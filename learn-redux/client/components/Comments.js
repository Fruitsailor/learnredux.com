import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import Comment from './Comment';

const Comments = (props) => (
    <div className='comment'>
        {props.postComments.map((comment, i) =>
            <Comment key={i} comment={comment} postId={props.params.postId} i={i} removeComment={props.removeComment} />
        )}
        <form className='comment-form' onSubmit={props.handleSubmit}>
            <Field name='author' type='text' component='input' placeholder='author' />
            <Field name='comment' type='text' component='input' placeholder='comment' />
            <input type='submit' hidden/>
        </form>
    </div>
);

Comments.PropTypes = {
        removeComment: PropTypes.func.isRequired,
        params: PropTypes.shape({
            postId: PropTypes.string.isRequired,
        }),
        postComments: PropTypes.func.isRequired,
        handleSubmit: PropTypes.func.isRequired,

}

export default reduxForm({form: 'commentForm'})(Comments);